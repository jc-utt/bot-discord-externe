require('dotenv').config();

const { Client, GatewayIntentBits } = require('discord.js');
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessageReactions, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });

const guildID = "805731220830945290", channelID = "1082287631116795925";
const messageContent = "Bienvenue à toi sur le serveur de Junior Conseil UTT, si tu cherches à réaliser une mission qui pourra te faire monter en compétences mais également gagner de l’argent tu es au bon endroit ! \nReact aux emojis qui correspondent à tes compétences pour qu’on puisse te tagger en fonction des missions qui sont disponibles :\nCréation de site internet 💻 \nModification de base de données 📟 \nEtat de l’art   📜 \nApplication mobile 📳 \nDébug ⚠️ \nCodage de machine 🤖 \nCAO 🧑🏻‍🔧 \nOptimisation des systèmes 📂 \nLogiciel ( app ) 🈸 \nCybersécurité 🩹 \nCycle de vie ( écoconception ) ♻️\nWeb éco ☘️\n\nJ’espère qu’on pourra te trouver une mission le plus rapidement possible ! "
const roles = {"💻":"1082319758898249748", "📟":"1082320062490345472", "📜":"1082320102042640434", "📳":"1082320142135988274", "⚠️":"1082320169981972500", "🤖":"1082320202227777656", "🧑🏻‍🔧":"1082320235350216774", "📂":"1082320267201740891", "🈸":"1082320294020128768", "🩹":"1082320324605005954", "♻️":"1082320359048630322", "☘️":"1082320395782328421"}

let guild, channel, message
client.on('ready', async () => {
  console.log(`Logged in as ${client.user.tag}!`);
  guild = await client.guilds.fetch(guildID);
  channel = await guild.channels.fetch(channelID);
  console.log("retreived guild and channel")
  messages = (await channel.messages.fetch()).filter(msg=>msg.author.id == client.user.id)
  messages.each(msg=>msg.delete())
  message = await channel.send(messageContent)
  Object.keys(roles).forEach(r=>message.react(r))
});
client.on("messageReactionAdd", async (reaction, user)=>{
  if(user.bot)return
  if(!reaction.message.equals(message))return
  let member = guild.members.cache.filter(m=>m.user.id == user.id).first()
  let role = await guild.roles.fetch(roles[reaction.emoji.name])
  await member.roles.add(roles[reaction.emoji.name])	
  console.log("added \"" + role.name + "\" to " + member.user.tag)
})

client.on("messageReactionRemove", async (reaction, user)=>{
  if(user.bot)return
  if(!reaction.message.equals(message))return
  let member = guild.members.cache.filter(m=>m.user.id == user.id).first()
  let role = await guild.roles.fetch(roles[reaction.emoji.name])
  await member.roles.remove(roles[reaction.emoji.name])	
  console.log("removed \"" + role.name + "\" from " + member.user.tag)

})

client.login(process.env.DISCORD_TOKEN);
